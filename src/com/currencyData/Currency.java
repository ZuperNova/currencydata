package com.currencyData;
import org.json.JSONObject;
import java.net.URL;
import java.net.URLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Currency {

    private String[] fields = {"ask", "bid"};
    private HashMap<String, HashMap<String, String>> saveData;
    private HashMap<String, HashMap<String, String>> currencyData = new HashMap<String, HashMap<String, String>>();
    private HashMap<String, String> values;
    private String[] headers;
    private String[] newHeaders;

    protected String requestURL(String url) throws Exception{
        URLConnection connection = new URL(url).openConnection();
        connection.setRequestProperty("User-Agent",
                "Mozilla/5.0 (Windows NT 6.1; WOW64) " +
                "AppleWebKit/537.11 (KHTML, like Gecko)" +
                "Chrome/23.0.1271.95 Safari/537.11");

        connection.connect();
        BufferedReader buffer  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = buffer.readLine()) != null) {
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }

    protected void setHeaders(String... headers) {
        this.headers = headers;
    }

    protected String[] getHeaders() {
        return newHeaders;
    }

    protected HashMap<String, HashMap<String, String>> getCurrencyData() throws Exception{

        currencyData = new HashMap<String, HashMap<String, String>>();
        for(int i = 0; i < headers.length; i++) {
            values = new HashMap<String, String>();
            String url = "https://financialmodelingprep.com/api/v3/forex/" + headers[i];
            JSONObject jo = new JSONObject(requestURL(url));
            values.put("bid", jo.get("bid").toString());
            values.put("ask", jo.get("ask").toString());
            currencyData.put(jo.get("ticker").toString(), values);

        }
        return currencyData;
    }

    protected void saveCurrencyData(HashMap<String,HashMap<String, String>> data) {
        this.saveData = data;
    }

    protected HashMap<String,HashMap<String, String>> getSaveData() {
        return this.saveData;
    }

    protected HashMap<String, HashMap<String, String>> getChange() throws Exception {

        HashMap<String, String> values;
        String newHeader = "";
        newHeaders = new String[headers.length];
        for(int i = 0; i < headers.length; i++) {
            String str = headers[i];
             newHeader = str.substring(0,3) + "/" + str.substring(3);
             newHeaders[i] = newHeader;
        }

        if(currencyData.size() < 1) {
            HashMap<String, HashMap<String, String>> first = getCurrencyData();

            for(int i = 0; i < newHeaders.length; i++) {
                values = first.get(newHeaders[i]);
                values.put("bid", "#" + values.get("bid"));
                values.put("ask", "#" + values.get("ask"));
                first.get(newHeaders[i]).put("bid", values.get("bid"));
                first.get(newHeaders[i]).put("ask", values.get("ask"));
            }

            return first;
        }

        for(int i = 0; i < newHeaders.length; i++) {
            String bid = currencyData.get(newHeaders[i]).get("bid").substring(1);
            String ask = currencyData.get(newHeaders[i]).get("ask").substring(1);
            currencyData.get(newHeaders[i]).put("bid", bid);
            currencyData.get(newHeaders[i]).put("ask", ask);
        }

        saveCurrencyData(currencyData);
        HashMap<String, HashMap<String, String>> saved = getSaveData();
        HashMap<String, HashMap<String, String>> current = getCurrencyData();


        for(int i = 0; i < newHeaders.length; i++) {
            double savedBid = Double.parseDouble(saved.get(newHeaders[i]).get("bid"));
            double savedAsk = Double.parseDouble(saved.get(newHeaders[i]).get("ask"));

            double currentBid = Double.parseDouble(current.get(newHeaders[i]).get("bid"));
            double currentAsk = Double.parseDouble(current.get(newHeaders[i]).get("ask"));

            double changeBid = currentBid - savedBid;
            double changeAsk = currentAsk - savedAsk;

            if(Math.signum(changeBid) == 1.0) {
                current.get(newHeaders[i]).put("bid", "+" + current.get(newHeaders[i]).get("bid"));
            } else if(Math.signum(changeBid) == -1.0) {
                current.get(newHeaders[i]).put("bid", "-" + current.get(newHeaders[i]).get("bid"));
            } else {
                current.get(newHeaders[i]).put("bid", "#" + current.get(newHeaders[i]).get("bid"));
            }

            if(Math.signum(changeAsk) == 1.0) {
                current.get(newHeaders[i]).put("ask", "+" + current.get(newHeaders[i]).get("ask"));
            } else if(Math.signum(changeAsk) == -1.0) {
                current.get(newHeaders[i]).put("ask", "-" + current.get(newHeaders[i]).get("ask"));
            } else {
                current.get(newHeaders[i]).put("ask", "#" + current.get(newHeaders[i]).get("ask"));
            }
        }
        return current;
    }

}
