package com.currencyData;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) throws Exception{

        System.out.println("Håkon Underdal, Gunnar Sigstad, Truls Gunnerød");

        Currency currency = new Currency();
        // Here you can add currency
        currency.setHeaders("USDSEK", "USDNOK", "USDJPY", "EURUSD");

        while(true) {
            HashMap<String, HashMap<String, String>> data = currency.getChange();
            String[] headers = currency.getHeaders();
            String[] header = new String[headers.length + 1];
            String[][] elements = new String[2][headers.length+1];
            header[0] = "#";
            elements[0][0] = "Ask";
            elements[1][0] = "Bid";
            for(int i = 1; i < header.length; i++) {
                header[i] = headers[i-1];
            }

            for(int i = 1; i < header.length; i++) {
                elements[0][i] = data.get(header[i]).get("ask");
            }

            for(int i = 1; i < header.length; i++) {
                elements[1][i] = data.get(header[i]).get("bid");
            }

            Table table = new Table(header, elements);

            System.out.print("\033[H\033[2J");
            System.out.flush();

            System.out.println(table.createHeaderRow());
            System.out.println(table.createCells());

            try{
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                System.err.println("Something went wrong...");
            }
        }
    }
}
