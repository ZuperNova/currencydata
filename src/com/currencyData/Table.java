package com.currencyData;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Formatter;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class Table {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_GREEN = "\u001B[32m";
    private static final String ANSI_RESET = "\u001B[0m";

    private List<String> headers;
    private List<List<String>> cells;
    private final int LENGTH_OF_LARGEST_WORD;
    private final int CELL_HORIZONTAL_MARGIN = 4;

    Table(String [] headers, String [][] elements) {
        this.headers = new ArrayList<>(Arrays.asList(headers));
        this.cells = new ArrayList<>();
        for(String row[] : elements) {
            this.cells.add(Arrays.asList(row));
        }
        this.LENGTH_OF_LARGEST_WORD = findLargestWord();
    }

    public String createHeaderRow() {

        /* CREATE TOP LINE */
        String topLineFormat = IntStream.range(0, LENGTH_OF_LARGEST_WORD+CELL_HORIZONTAL_MARGIN).mapToObj(i -> "-").collect(Collectors.joining()) + "+";
        String topLine = "+";
        for(int i = 0; i < headers.size(); i++) {
            topLine += topLineFormat;
        }

        /* CREATE HEADER CELL */
        String centerAlignFormat = " %-" + (LENGTH_OF_LARGEST_WORD+1) + "s |";
        String headerCells = "|";

        for(int i = 0; i < headers.size(); i++) {
            int paddingLeft = (CELL_HORIZONTAL_MARGIN + LENGTH_OF_LARGEST_WORD - headers.get(i).length() )/2;
            int paddingRight = paddingLeft + (CELL_HORIZONTAL_MARGIN + LENGTH_OF_LARGEST_WORD - headers.get(i).length() )%2;
            // left padding
            for(int j = 0; j < paddingRight; j++ ) {
                headerCells = headerCells.concat(" ");
            }
            headerCells = headerCells.concat(headers.get(i));
            // right padding
            for(int j = 0; j < paddingLeft; j++) {
                headerCells = headerCells.concat(" ");
            }
            headerCells = headerCells.concat("|");
        }
        return topLine + "\n" + headerCells + "\n" + topLine;
    }

    public String createCells() {
        String bottomLine = "+", cell = "|";

        String bottomLineFormat = IntStream.range(0, LENGTH_OF_LARGEST_WORD+CELL_HORIZONTAL_MARGIN).mapToObj(i -> "-").collect(Collectors.joining()) + "+";
        for(int i = 0; i < cells.get(0).size(); i++) {
            bottomLine += bottomLineFormat;
        }

        for(int row = 0; row < cells.size(); row++) {
            for(int element = 0; element < cells.get(row).size(); element++) {
                int paddingLeft = (CELL_HORIZONTAL_MARGIN + LENGTH_OF_LARGEST_WORD - trimValue( cells.get(row).get(element)).length() )/2;
                int paddingRight = paddingLeft + (CELL_HORIZONTAL_MARGIN + LENGTH_OF_LARGEST_WORD - trimValue( cells.get(row).get(element)).length() )%2;

                for(int j = 0; j < paddingRight; j++ ) {
                    cell = cell.concat(" ");
                }
                cell = cell.concat( getValueString(cells.get(row).get(element)) );
                // right padding
                for(int j = 0; j < paddingLeft; j++) {
                    cell = cell.concat(" ");
                }
                cell = cell.concat("|");
            }
            cell += "\n" + bottomLine + "\n" + (row+1 < cells.size() ? "|" : "");
        }

        return cell;
    }

    private String getValueString(String value) {
        if(value.charAt(0) == '-') {
            return ANSI_RED + value.substring(1, value.length()) + ANSI_RESET;
        }
        else if(value.charAt(0) == '+') {
            return ANSI_GREEN + value.substring(1, value.length()) + ANSI_RESET;
        }
        else if(value.charAt(0) == '#') {
            return value.substring(1, value.length());
        }
        else {
            return value;
        }
    }
    // Returns a String without the sign
    private String trimValue(String val) {
        if(val.charAt(0) == '-' || val.charAt(0) == '+' || val.charAt(0) == '#')
            return val.substring(1, val.length());
        return val;
    }

    /*
     * Find the largest word and return the number of characters in that word
     * */
    private int findLargestWord() {
        int largestWord = 0;
        for(String header : this.headers) {
            if(header.length() > largestWord)
                largestWord = header.length();
        }
        for(List<String> row : cells) {
            for(String cell : row) {
                if(cell.length() > largestWord)
                    largestWord = cell.length();
            }
        }
        return largestWord;
    }
    public void setHeader(String h) {
        this.headers.set(0, h);
    }
}